import React, { Component } from "react";
import "./styles.css";

/*

По клику на компонент Button должен
показываться компонент Alert,
он должен просто показать текст, который
приходит в пропс message и с цветом пропса color

*/


function Message(props) {
  return(
    <div style = {{color: props.color}}>{props.text}</div>
  )
}

function MyButton(props){
  return(
    <button onClick = {props.onClick}>{props.content}</button>
  )
}

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      isHide: false
    }
  }
  render() {
    return (
      <div className="App">
        <MyButton content = "Click" onClick = {() => (this.setState({isHide: !this.state.isHide}))}/>
         {this.state.isHide && <Message text = "Hello, world" color = "red"/>}
      </div>
    );
  }
}

export default App;
